<!DOCTYPE html>

<!--
   contact.php
   
   Kael Fraga, Pablo Diehl
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
-->

<?php 
	if(isset($_POST['submit'])){
    		$destino = "minusonegames@mail.com"; 
    		$usuario = $_POST['email'];
    
		$nome = $_POST['nome'];
		$assunto = $_POST['assunto'];    		

		$tituloDrago = "[Dragoste] " . $assunto;
    		$tituloUsuario = "[Dragoste] Cópia da sua mensagem";
    
		$mensagemDrago = $nome . " enviou uma mensagem através do formuário de contato da Dragoste: \n\n\n" . $_POST['mensagem'];
    		$mensagemUsuario = $nome . " esta é uma cópia da mensagem enviada por você através do formulário de contato da Dragoste, retornaremos assim que possível. \n\n\n" . $_POST['mensagem'];

    		$cabecalhoDrago = "From:" . $usuario;
    		$cabecalhoUsuario = "From:" . $destino;
    
		mail($destino,$tituloDrago,$mensagemDrago,$cabecalhoDrago);
    		mail($usuario,$tituloUsuario,$mensagemUsuario,$cabecalhoUsuario);
    		echo '<script>alert("Mensagem enviada com sucesso!")</script>';
    }
?>

<html>
    <head>
        <title>Fale com a Dragoste</title>
        <meta charset="UTF-8">
        <link href='Estilos/estilo.css' rel='stylesheet' type='text/css'>
    </head>
    <body>        
        <?php include('header.php'); ?>
        
        <div class="clear pagina">
            <h1 id="faleConosco">Fale com a Dragoste</h1>	
            <div id="contato" class="dragosteDiv">
                <h1 id="tituloContato">Contato</h1>
                <div id="formularioContato">    
                    <form method="POST" action="" id="formulario" name="formulario">                
                        <label for = "nome">Nome:</label><br>
                        <input type="text" name="nome" id="nome" required>
                        <br>
                        <br>						    
                        <label for = "email">E-Mail:</label><br>
                        <input type="email" name="email" id="email" required>
                        <br>
                        <br>
                        <label for = "assunto">Assunto:</label><br>
                        <input type="text" name="assunto" id="assunto"><br>
                        <br>
                        <label for = "mensagem">Mensagem:</label><br>
                        <textarea name="mensagem" id="mensagem" required></textarea><br>
                        <br>
                        <input class="dragosteButton" type="submit" value="Enviar mensagem" id = "sendBut" name="submit" onclick="return validador()">
                    </form>
                </div>
            </div>

            <div id="info" class="dragosteDiv">
                <h1 id="tituloInfo">Informações</h1>
                <p>
                    <span>Desenvolvedores:</span><br>
                    Kael Fraga - <a href="mailto:kaelfraga@hotmail.com">kaelfraga@hotmail.com</a><br>
                    Pablo Diehl - <a href="mailto:pablodiehl@ymail.com">pablodiehl@ymail.com</a>
                </p>
                <p>
                    <span>Professor:</span><br>
                    Rafael Pinto - <a href="mailto:rafael.pinto@canoas.ifrs.edu.br">rafael.pinto@canoas.ifrs.edu.br</a>
                </p>
                <p>
                    <span>Disciplina:</span><br>
                    Linguagem de Programação para Web
                </p>
                <p>
                    <span>Curso:</span><br>
                    Superior de Tecnologia em Análise e Desenvolvimento de Sistemas
                </p>
                <p>
                    <span>Semestre:</span><br>
                    2015/1
                </p>
                <p>
                    <span>Localização:</span>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3458.7940624612734!2d-51.150540999999954!3d-29.899033999999993!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x951970265f07cfb5%3A0xa38d71cab38e0da1!2sIFRS+-+C%C3%A2mpus+Canoas!5e0!3m2!1spt-BR!2sbr!4v1429584455806"></iframe>
                </p>
            </div>

           <?php include('footer.php'); ?>
            
        </div>
        <script>
            "use strict";

		function validaEmail(email) {
        	        var arrei = email.split('@');
	
                	if (arrei.length !== 2) {
        	            return false;
	                } else {
        	            var subarrei = arrei[1].split('.');
                	    if (subarrei.length < 2) {
                        	return false;
                    	    }
                	}
                	return true;
            	}

            function validador() {
                var nome = formulario.nome.value;
                var email = formulario.email.value;
                var mensagem = formulario.mensagem.value;

                if (nome === "") {
                    alert('Preencha o campo "Nome".');
                    formulario.nome.focus();
                    return false;
                }

                if (!validaEmail(email)) {
                    alert('Email inválido.');
                    formulario.email.focus();
                    return false;
                }

                if ((mensagem.length < 5) || (mensagem.length > 500)) {
                    alert('A mensagem deve ter entre 5 e 500 caracteres.');
                    formulario.senha.focus();
                    return false;
                }

                return true;
            }
        </script>
    </body>
</html>