<?php

/*
  Kael Fraga, Pablo Diehl

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 */
include("connect.php");

if (isset($_SESSION['cart'])) {
    if (isset($_GET['gameid'])):
        $this_game = selectGameInfo($_GET['gameid']);
        if ($this_game !== null):
            if (($key = array_search($this_game, $_SESSION['cart'])) !== false) {
                unset($_SESSION['cart'][$key]);
            }
        endif;
    endif;
}
header('Location: cart.php');
