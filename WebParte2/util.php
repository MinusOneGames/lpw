<?php

/*
  Kael Fraga, Pablo Diehl

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 */

function formatSQLDate($sqldate, $format = 'd/m/Y') {
    $phpdate = strtotime($sqldate);
    return date($format, $phpdate);
}

function formatValue($value) {
    return intval($value) > 0 ? "R$" . number_format($value, 2, ',', '.') : "Grátis!";
}

function validaEmail($email) {
    $array = split('@', $email);

    if (count($array) !== 2) {
        return false;
    } else {
        $sub_array = split('.', $array[1]);
        if (count($sub_array) < 2) {
            return false;
        }
    }
    return true;
}

function validaCaracteres($username) {
    if (preg_match('/^[a-zA-Z0-9]+/', $username)) {
        return true;
    }
    return false;
}

function validaDataNasc($dataNasc) {
    $arrei = split('/', $dataNasc);

    if (count($arrei) !== 3) {
        return false;
    } else {
        if ((count($arrei[0]) !== 2) || (count($arrei[1]) !== 2) || (count($arrei[2]) !== 4)) {
            return false;
        }
    }

    foreach ($arrei as $i) {
        if (intval($i) === 0){
            return false;
        }    
    }

    $day = intval($arrei[0]);
    $month = intval($arrei[1]);
    $year = intval($arrei[2]);

    if ($year < 1) {
        return false;
    } else if (($month < 1) || ($month > 12)) {
        return false;
    } else if (($month === 1) || ($month === 3) || ($month === 5) || ($month === 7) || ($month === 8) || ($month === 10) || ($month === 12)) {//mes com 31 dias
        if (($day < 1) || ($day > 31)) {
            return false;
        }
    } else if (($month === 4) || ($month === 6) || ($month === 9) || ($month === 11)) {//mes com 30 dias
        if (($day < 1) || ($day > 30)) {
            return false;
        }
    } else if ($month === 2) {//fevereiro e ano bissexto
        if (($year % 4 === 0) && (($year % 100 !== 0) || ($year % 400 === 0))) {
            if (($day < 1) || ($day > 29)) {
                return false;
            }
        } else {
            if (($day < 1) || ($day > 28)) {
                return false;
            }
        }
    }
       
    return true;
}

function validaCampos($campos) {

    $erros = null;

    if (!isset($campos["nome_real"]) || (strlen($campos["nome_real"]) === 0)):
        $erros[] = "Nome Real deve ser preenchido.";
    endif;

    if (!isset($campos["nome_usuario"]) || (strlen($campos["nome_usuario"]) < 3) || !validaCaracteres($campos["nome_usuario"])):
        $erros[] = "Nome de Usuário inválido,
                    deve conter ao menos 3 caracteres 
                    e não pode conter caracteres especias, nem espaços";
    endif;

    if (!isset($campos["email"]) || (strlen($campos["email"]) == 0) || !(validaEmail($campos["email"]))):
        $erros[] = "Email inválido.";
    endif;

    if (!isset($campos["senha"]) || (strlen($campos["senha"]) < 5)):
        $erros[] = "Senha inválida, deve possuir ao menos 5 caracteres.";
    endif;

    if ($campos["senha"] === $campos["nome_usuario"]):
        $erros[] = "Sua senha deve ser diferente de seu nome de usuário.";
    endif;

    if (!isset($campos["repete"]) || ($campos["senha"] !== $campos["repete"])):
        $erros[] = "As senhas informadas não são iguais.";
    endif;

    if (!isset($campos["data_nasc"]) || (strlen($campos["data_nasc"]) === 0) /*|| !validaDataNasc($campos["data_nasc"])*/):
        $erros[] = "Data de Nascimento inválida.";
    endif;

    if (!isset($campos["concordo"]) || (strlen($campos["concordo"]) === 0) || ($campos["concordo"]!=="check")):
        $erros[] = "Você precisa concordar com os termos para cadastrar-se na Dragoste.";
    endif;

    return $erros;
}
