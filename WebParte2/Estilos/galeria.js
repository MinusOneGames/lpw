var posicao = 0,
imagens = $('.novaGaleria div'),
qtdImgs = imagens.length;
        
function organiza() {
	var imagem = $('.novaGaleria div').eq(posicao);
    imagens.hide();
    imagem.css('display','inline-block');
}
          
$(window).load(function() {
	organiza();
});
          
function executaAuto() {
	posicao += 1;
    if (posicao > qtdImgs - 1) {
    	posicao = 0;
    }
    organiza();
}
          
var velocidade = 3000;
var automatico = setInterval(executaAuto, velocidade);
          
$('.proxima').click(function() {
	clearInterval(automatico);
    automatico = setInterval(executaAuto, velocidade);
    posicao += 1;
    if (posicao > qtdImgs - 1) {
    	posicao = 0;
    }
    organiza();
});
          
$('.anterior').click(function() {
	clearInterval(automatico);
    automatico = setInterval(executaAuto, velocidade);
    posicao -= 1;
    if (posicao < 0) {
    	posicao = qtdImgs - 1;
    }
    organiza();
});