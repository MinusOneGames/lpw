<!DOCTYPE html>
<!--
   login.php
   
   Kael Fraga, Pablo Diehl
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
-->

<html>
    <head>
        <title>Login</title>
        <meta charset="UTF-8">
        <link href='Estilos/estilo.css' rel='stylesheet' type='text/css'>	
    </head>

    <body>
        <?php
        include('header.php');

        if (isset($_SESSION['user'])):
            echo '<script> alert("Você já está logado!"); </script>';
            header('refresh:0.1; url=index.php');
        endif;
        ?>

        <div class="clear pagina">	
            <div id = "formLogin" class = "formDiv dragosteDiv">
                <h1>LOGIN</h1>      
                <form method="POST" action="connect.php">
                    <label for="user">Usuário:<br></label>
                    <input type="text" name="usuario" id = "user" required><br>
                    <label for="pass">Senha:<br></label>
                    <input type="password" id="pass" name="senha" required><br>         
                    <div>
                        <input type="checkbox" id= "savePass" name="salvarSenha">
                        <label for="savePass">Salvar senha</label>                     
                    </div>
                    <input class="dragosteButton" name = "loginBut" type="submit" value="Entrar" id = "loginBut"><br>
                    <a href="#">Esqueci minha senha.</a>
                </form>      
            </div>

            <div id = "regInvite" class = "formDiv dragosteDiv">
                <form method="GET" action="register.php">
                    <p>
                        Junte-se à Dragoste!
                    </p>
                    <p>
                        Ainda não possui conta na Dragoste?<br> 
                        Cadastre-se, é de graça!<br> 
                        Perfeita para aqueles que amam jogos indie!<br> 
                    </p>
                    <input class="dragosteButton" type="submit" value="Cadastrar-se" id = "registerBut">
                </form>
            </div>

            <?php include('footer.php'); ?>

        </div>
    </body>
</html>