<!DOCTYPE html>

<!--
   cart.html
   
   Kael Fraga, Pablo Diehl
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
-->

<html>

    <head>
        <title>Carrinho de compras</title>
        <meta charset="UTF-8">
        <link href='Estilos/estilo.css' rel='stylesheet' type='text/css'>
    </head>

    <body>
        <?php
        include('header.php');
        include('connect.php');

        $this_game = null;
        if (isset($_GET['gameid'])):
            $this_game = selectGameInfo($_GET['gameid']);
            if ($this_game === null):
                echo '<script> alert("Jogo não encontrado!"); </script>';
                header('refresh:0.1; url=index.php');
            else:                
                if(!isset($_SESSION['cart'])):
                    $_SESSION['cart'][] = $this_game;
                elseif (!in_array($this_game, $_SESSION['cart'])):
                    $_SESSION['cart'][] = $this_game;
                endif;
            endif;
        endif;
        ?>

        <div class="clear pagina">
            <div class="corpoCart">
                <table>
                    <thead>
                        <tr>
                            <th colspan=3 >
                                Carrinho de Compras
                            </th>
                        </tr>
                    </thead>
                    <tbody>				
                        <tr>
                            <td><span>Item</span></td>
                            <td><span>Descrição</span></td>
                            <td><span>Preço</span></td>
                        </tr>

                        <?php
                        $total = 0.0;
                        if (isset($_SESSION['cart'])) :
                            foreach ($_SESSION["cart"] as $item):
                                $img = selectImagesFromGame($item["id_jogo"])[0];
                                $total += floatval($item["preco"]);
                                echo
                                '<tr>
                                <td>' . $item["id_jogo"] . '</td>
                                    <td class="corpoCartDesc">
                                         <img src="Assets/Jogos/' . $img["url"] . '" alt="' . $item["titulo"] . '"/> ' . $item["titulo"] . ' 
                                    </td>
                                <td>
                                    ' . formatValue($item["preco"]) . '<br><a href="remove.php?gameid=' . $item["id_jogo"] . '">Remover do Carrinho</a>
                                </td>
                            </tr>';
                            endforeach;
                        endif;
                        ?>     
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan=2 ><a href="index.php"><div id="botaoContinuarComprando" class = "dragosteButton">Continuar Comprando</div></a></td>
                            <td colspan=2 id="corpoCartSubtotal">Total: <?php echo formatValue($total); ?></td>
                        </tr>
                    </tfoot>
                </table>
            </div>

            <div class="clear finalizarCompra">
                <a href="#"><div id="botaoFinalizarCompra">Finalizar Compra</div></a>
            </div>

            <?php include('footer.php'); ?>

        </div>
    </body>

</html>
