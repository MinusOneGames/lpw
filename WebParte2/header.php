<?php

/*
  Kael Fraga, Pablo Diehl

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 */

if (!isset($_SESSION)): 
    session_start();
endif;

$output = '<div id = "header">
            <ul id = "mainMenu" class = "horizontalMenu">
                <li>          
                    <a href="index.php"><img class = "logo" title = "Dragoste Logo" src = "Assets/dragoste_logo.svg" alt="Dragoste Logo" ></a>
                </li>
                <li><a href="index.php">DESTAQUES</a></li>
                <li><a href="contact.php">CONTATO</a></li>
                <li><a href="register.php">CADASTRAR</a></li>
                <li><a href="cart.php">CARRINHO</a></li>';

if (isset($_SESSION['user'])) {
    $output = $output . '<li><a href = "logout.php" onclick="return confirm(\'Tem certeza que deseja sair?\');">LOGOUT</a></li>';
    $output = $output . '<li id="username">USUÁRIO: ' . $_SESSION['user'] . '</li>';
} else {
    $output = $output . '<li><a href = "login.php">LOGIN</a></li > ';
}

$output = $output . '<li>
                        <div id = "searchbox">
                            <form>
                                <input type = "text" name = "searchInput" maxlength = "500" value = "" class = "text" id = "sitesearch" placeholder = "Search ..."/>
                               <input type = "image" name = "searchIcon" src = "./Assets/btn_search_box.png" class = "button" alt = "Search Dragoste" />
                            </form>
                        </div>
                    </li>
                </ul>
            </div>';

echo $output;
