<!DOCTYPE html>

<!--
   index.php
   
   Kael Fraga, Pablo Diehl
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
-->

<html>

    <head>
        <title>Seja bem vindo a Dragoste! ;)</title>
        <meta charset="UTF-8">
        <link href='Estilos/estilo.css' rel='stylesheet' type='text/css'>
    </head>

    <body>
        <?php
        include('header.php');
        include('connect.php');

        $jogos = selectAllGames();
        $destaque = $jogos[0];
        unset($jogos[0]);        
        $img_promo = "Assets/Jogos/" . selectImagesFromGame($destaque["id_jogo"])[0]["url"];
        
        ?>

        <div class="clear pagina">            
            <div id = "presentation">
                Bem vindo a Dragoste, a loja que representa seu amor por jogos indie!<br>
            </div>      

            <div class="promo" 
                 style="background-image: url(<?php echo $img_promo; ?>);">

                <p class="textPromo">Hiper destaque do dia</p>                

                <?php
                echo '<a href="game.php?gameid=' . $destaque["id_jogo"] . '">
                      <div class = "dragosteButton">Visitar a página de ' .
                      $destaque["titulo"] . '!</div></a>';
                ?>                
            </div>
            
            <div class = "verticalMenu categoryMenu">
                <ul>
                    <li><span>GÊNERO</span></li>	
                    <?php
                    $categorias = selectAllCategories();
                    foreach ($categorias as $categoria):
                        echo '<li><a href="#">' .
                        $categoria["descricao"] .
                        '</a></li>';
                    endforeach;
                    ?>         
                </ul>
            </div>

            <div class = "corpoPrincipal">
                <h1>Jogos em Destaque</h1>
                <?php
                if ($jogos):
                    foreach ($jogos as $jogo):
                        $img = selectImagesFromGame($jogo["id_jogo"])[0];
                        echo
                        '<div id = "imagem_div">
                                <a href="game.php?gameid=' . $jogo["id_jogo"] . '">
                                    <img src="Assets/Jogos/' . $img["url"] .
                        '" alt="' . $jogo["titulo"] . ' imagem ' . $img["id_imagem"] . '"/>
                                    <p><span>' . $jogo["titulo"] . '</span></p>
                                    <p>' . formatValue($jogo["preco"]) . '</p>    
                                </a>
                            </div>';
                    endforeach;
                endif;
                ?>
            </div>

            <?php include('footer.php'); ?>
        </div>
    </body>
</html>