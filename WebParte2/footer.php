<?php

/*
  Kael Fraga, Pablo Diehl

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 */

$output = '<div id="footer" class = "clear">
        <ul id = "botMenu" class = "horizontalMenu">
            <li><a href="index.php">DESTAQUES</a></li>';

if (isset($_SESSION['user'])) {
    $output = $output . '<li><a href="logout.php" onclick="return confirm(\'Tem certeza que deseja sair?\');">LOGOUT</a></li>';
} else {
    $output = $output . '<li><a href="login.php">LOGIN</a></li>';
}

$output = $output . '<li><a href="register.php">CADASTRAR</a></li>
            <li><a href="contact.php">CONTATO</a></li>                    
            <li><a href="cart.php">CARRINHO</a></li>
            <li><a href="https://www.facebook.com/minusonegames"><img src="Assets/ico_facebook.gif" title = "Página do Facebook" alt = "Facebook:"> MINUS ONE GAMES</a></li>
            <li><a href="https://twitter.com/minusonegames"><img src="Assets/ico_twitter.gif" title = "Página do Twitter" alt = "Twitter:"> @MinusOneGames</a></li>
        </ul>
        <br class ="clear">

        <div class="copyright">
            &copy; 2015 <a href="http://minusonegames.weebly.com/">Minus One Games</a>.
         </div>
      </div>';

echo $output;
