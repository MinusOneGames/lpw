<!DOCTYPE html>

<!--   
   Kael Fraga, Pablo Diehl
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
-->

<html>

    <?php
    $this_game = null;
    include('connect.php');

    if (isset($_GET['gameid'])):
        $this_game = selectGameInfo($_GET['gameid']);
    endif;

    if ($this_game === null):
        echo '<script> alert("Jogo não encontrado!"); </script>';
        header('refresh:0.1; url=index.php');
    endif;
    ?>

    <head>
        <title><?php echo $this_game["titulo"]; ?> na Dragoste</title>
        <meta charset="UTF-8">
        <link href='Estilos/estilo.css' rel='stylesheet' type='text/css'>	
    </head>

    <body>
        <?php include('header.php'); ?>

        <div class="clear pagina">	
            	<h1 class="novaGaleriaTitulo"><?php echo $this_game["titulo"]; ?></h1>
		<div class="quadro">
                	<div class="novaGaleria">
				<button class="anterior"> ← </button>
                  		<button class="proxima"> → </button>
                    		<?php
                    			$galeria = selectImagesFromGame($_GET['gameid']);
                    			if ($galeria):
                        			foreach ($galeria as $img):
                            				echo'<div style="display:none;"><img src="Assets/Jogos/' . $img["url"] .'" alt="' . $this_game["titulo"] . ' imagem ' . $img["id_imagem"] . '"/></div>';
                     				endforeach;
                    			endif;
                    		?>
                	</div>
            	</div>

            <div class="detalhes">
                <p>Detalhes do Jogo</p>
                <ul>
                    <li><span>Título:</span> <?php echo $this_game["titulo"]; ?></li>
                    <li><span>Gênero:</span> <?php echo $this_game["genDesc"]; ?></li>
                    <li><span>N° mínimo de Jogadores:</span> <?php echo $this_game["min_jogadores"]; ?></li>
                    <li><span>N° máximo de Jogadores:</span> <?php echo $this_game["max_jogadores"]; ?></li>
                    <li><span>Desenvolvedor:</span> <?php echo $this_game["desenv"]; ?></li>
                    <li><span>Data de Lançamento:</span> <?php echo formatSQLDate($this_game["lancamento"], "d/m/Y"); ?></li>
                    <li><span>Preço: </span> <?php echo formatValue($this_game["preco"]); ?></li>
                </ul>
                
                <?php echo '<a href="cart.php?gameid='. $this_game["id_jogo"] .'"><div id="botaoComprar" class = "dragosteButton">Comprar</div></a>'; ?>                      
            </div>

            <div class="descricao">
                <div id="tituloDesc">Descrição</div>                
                <div id="trailer">                    
                    <?php echo '<iframe width="560" height="315" src="https://www.youtube.com/embed/'.$this_game["video"].'?rel=0" allowfullscreen></iframe>' ?>  
                </div>
                <div class="descText">
                    <p><?php echo $this_game["gameDesc"]; ?></p>
                    <br>
                </div>
            </div>

            <?php include('footer.php'); ?>
        </div>
	<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="./Estilos/galeria.js"></script>
    </body>

</html>