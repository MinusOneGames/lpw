<!DOCTYPE html>
<!--
   register.php
   
   Kael Fraga, Pablo Diehl
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
-->

<html>
    <head>
        <title>Cadastro</title>
        <meta charset="UTF-8">
        <link href='Estilos/estilo.css' rel='stylesheet' type='text/css'>	
    </head>

    <body>
        <?php include('header.php'); ?>

        <div class="clear pagina">	
            <div id = "formRegister" class = "formDiv dragosteDiv">
                <h1>CADASTRO</h1> 
                <form method="POST" action="connect.php" id="formulario" name="form_cadastro">

                    <label for = "nome_real">Nome:</label><br>
                    <input id="nome_real" type="text" name="nome_real" required><br>

                    <label for = "nome_usuario">Nome de usuário:</label><br>
                    <input id="nome_usuario" type="text" name="nome_usuario" required><br>

                    <label for = "email">E-mail:</label><br>
                    <input id="email" type="email" name="email" required><br>

                    <label for = "senha">Senha:</label><br>
                    <input id="senha" type="password" name="senha" required><br>

                    <label for = "repete">Confirme a Senha:</label><br>
                    <input id="repete" type="password" name="repete" required><br>

                    <label for = "data_nasc">Data de nascimento:</label><br>
                    <input id="data_nasc" type="date" name="data_nasc" required><br>

                    <div>
                        <input id = "concordo" type="checkbox" name="concordo" value="check">
                        <label for = "concordo">Concordo com os <a href = "#">Termos e Condições de Uso</a>.</label><br> 
                    </div>
  
                    <input class="dragosteButton" type="submit" value="Salvar e enviar" name="cadastrar" id = "saveRegisterBut" onclick="return validador()">
                </form>      
            </div>
            
            <?php include('footer.php'); ?>
            
        </div>
        <script>
            "use strict";
            function validaEmail(email) {
                var arrei = email.split('@');

                if (arrei.length !== 2) {
                    return false;
                } else {
                    var subarrei = arrei[1].split('.');
                    if (subarrei.length < 2) {
                        return false;
                    }
                }
                return true;
            }

            function validaDataNasc(dataNasc) {
                var arrei = dataNasc.split('/');

                if (arrei.length !== 3) {
                    return false;
                } else {
                    if ((arrei[0].length !== 2) || (arrei[1].length !== 2) || (arrei[2].length !== 4)) {
                        return false;
                    }
                }

                var i;
                for (i in arrei) {
                    if (isNaN(parseInt(arrei[i])))
                        return false;
                }

                var day = Number(arrei[0]);
                var month = Number(arrei[1]);
                var year = Number(arrei[2]);

                if (year < 1) {
                    return false;
                } else if ((month < 1) || (month > 12)) {
                    return false;
                } else if ((month === 1) || (month === 3) || (month === 5) || (month === 7) || (month === 8) || (month === 10) || (month === 12)) {//mes com 31 dias
                    if ((day < 1) || (day > 31)) {
                        return false;
                    }
                } else if ((month === 4) || (month === 6) || (month === 9) || (month === 11)) {//mes com 30 dias
                    if ((day < 1) || (day > 30)) {
                        return false;
                    }
                } else if (month === 2) {//fevereiro e ano bissexto
                    if ((year % 4 === 0) && ((year % 100 !== 0) || (year % 400 === 0))) {
                        if ((day < 1) || (day > 29)) {
                            return false;
                        }
                    } else {
                        if ((day < 1) || (day > 28)) {
                            return false;
                        }
                    }
                }

                return true;
            }
            
            function validaCaracteres(nome){
                var especiais = '[^a-zA-Z0-9]+';
                
                if(nome.match(especiais)) {
                    return false;
                }
                return true;
            }

            function validador() {
                var nome_usuario = formulario.nome_usuario.value;
                var nome_real = formulario.nome_real.value;
                var email = formulario.email.value;
                var senha = formulario.senha.value;
                var repete = formulario.repete.value;
                var data_nasc = formulario.data_nasc.value;
                var concordo = document.getElementById("concordo");
                

                if (nome_real === "") {
                    alert('Preencha o campo "Nome".');
                    formulario.nome_real.focus();
                    return false;
                }

                if (nome_usuario.length < 3) {
                    alert('Seu nome de usuário deve ter pelo menos 3 caracteres.');
                    formulario.nome_usuario.focus();
                    return false;
                }
                
                if(!validaCaracteres(nome_usuario)){
                    alert('Seu nome de usuário não pode conter caracteres especias nem espaços.');
                    formulario.nome_usuario.focus();
                }

                if (!validaEmail(email)) {
                    alert('Email inválido.');
                    formulario.email.focus();
                    return false;
                }

                if (senha.length < 5) {
                    alert('A senha deve possuir ao menos 5 caracteres.');
                    formulario.senha.focus();
                    return false;
                }

                if (senha !== repete) {
                    alert('As senhas informadas não são iguais!');
                    formulario.repete.focus();
                    return false;
                }
                
                if (senha === nome_usuario) {
                    alert('Sua senha deve ser diferente de seu nome de usuário.');
                    formulario.senha.focus();
                    return false;
                }

                if (!validaDataNasc(data_nasc)) {
                    alert('Data de Nascimento inválida.');
                    formulario.data_nasc.focus();
                    return false;
                }
                
                if(!concordo.checked){
                    alert('Você precisa concordar com os termos para cadastrar-se na Dragoste.');
                    return false;
                }

                return true;
            }
        </script>
    </body>
</html>