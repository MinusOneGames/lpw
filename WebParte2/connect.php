<?php

/*
  Kael Fraga, Pablo Diehl

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 */

include('util.php');

if (!isset($_SESSION)):
    session_start();
endif;

$con = @mysql_connect('mysql.hostinger.com.br', 'u575094978_zuli', '12qwaszx');

function iterResults($sqlresult) {
    $resultados = null;
    if ($sqlresult):
        while ($linha = mysql_fetch_assoc($sqlresult)):
            $resultados[] = $linha;
        endwhile;
    endif;

    return $resultados;
}

function selectAllCategories() {
    if (mysql_select_db('u575094978_minus')):
        $resultado = mysql_query("SELECT * FROM Genero g");

        return iterResults($resultado);
    endif;

    return null;
}

function selectAllGames() {
    if (mysql_select_db('u575094978_minus')):
        $resultado = mysql_query("SELECT *, 
                                 j.descricao AS gameDesc, 
                                 g.descricao AS genDesc 
                                 FROM Jogo j 
                                 INNER JOIN Genero g 
                                 ON j.id_genero = g.id_genero 
                                 ORDER By j.prioridade LIMIT 7");

        return iterResults($resultado);
    endif;

    return null;
}

function selectGameInfo($gameID) {
    if (mysql_select_db('u575094978_minus') && (intval($gameID) != 0)):
        $linha = null;
        $resultado = mysql_query("SELECT *, 
                                 j.descricao AS gameDesc, 
                                 g.descricao AS genDesc,
                                 v.url AS video
                                 FROM Jogo j
                                 INNER JOIN Genero g ON j.id_genero = g.id_genero
                                 INNER JOIN Video v ON v.id_jogo = j.id_jogo
                                 AND j.id_jogo = " . $gameID);

        if ($resultado && mysql_num_rows($resultado) > 0):
            $linha = mysql_fetch_assoc($resultado);
        endif;

        return $linha;
    endif;

    return null;
}

function selectImagesFromGame($gameID) {
    if (mysql_select_db('u575094978_minus') && (intval($gameID) != 0)):
        $resultado = mysql_query("SELECT i.* FROM Imagem i
                                 INNER JOIN Jogo j ON i.id_jogo = j.id_jogo
                                 AND j.id_jogo = " . $gameID . " ORDER BY i.url");
        return iterResults($resultado);
    endif;

    return null;
}

function cadastrar() {

    $erros = validaCampos($_POST);
    $error_msg = "";

    if (count($erros) === 0):
        mysql_query("INSERT INTO Usuario (data_nasc, email, senha, nome_usuario, nome_real)
                            VALUES (" . $_POST["data_nasc"] . ",
				    '" . $_POST["email"] . "',
                                    '" . md5($_POST['senha']) . "',
                                    '" . $_POST["nome_usuario"] . "',
                                    '" . $_POST["nome_real"] . "')");
        $_SESSION['user'] = $_POST["nome_usuario"];
        echo '<script> alert("Usuário ' . $_POST["nome_usuario"] . ' cadastrado com sucesso!"); </script>';
    else:
        foreach ($erros as $erro):
            $error_msg = $error_msg . $erro . "\n";
        endforeach;
        echo $error_msg;
        echo '<script> alert("' . $error_msg . '"); </script>';
    endif;
}

function login() {
    $usuario = $_POST['usuario'];
    $senha = md5($_POST['senha']);

    if (($usuario != null) && ($usuario != '') && ($senha != null) && ($senha != '')) {
        $resultado = mysql_query("SELECT * FROM Usuario WHERE nome_usuario = '" . $usuario . "' AND senha = '" . $senha . "'");
        if (mysql_num_rows($resultado) > 0) {
            $_SESSION['user'] = $_POST['usuario'];
            echo '<script> alert("' . $_SESSION['user'] . ', seja bem vindo!"); </script>';
        } else {
            echo '<script> alert("Erro: Login inválido."); </script>';
        }
    }
}

if (mysql_select_db('u575094978_minus')) {
    if (isset($_POST['loginBut'])) {
        login();
        header('refresh:0.1; url=index.php');
    } elseif (isset($_POST['cadastrar'])) {
        cadastrar();
        header('refresh:0.1; url=index.php');
    }
}